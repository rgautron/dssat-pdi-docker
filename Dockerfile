FROM debian:bullseye as builder

USER 0

# this is here just to counter a lack in the base image
ENV BASH_ENV=/etc/profile
SHELL ["/bin/bash", "-c"]
ENTRYPOINT ["/bin/bash", "-c"]
CMD ["exec", "/bin/bash", "-li"]

RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install -y build-essential cmake gfortran python-dev python3-dev python3-venv python3-pip git pkg-config wget libyaml-dev

RUN wget http://pyyaml.org/download/libyaml/yaml-0.2.5.tar.gz
RUN tar -xf yaml-0.2.5.tar.gz
WORKDIR /yaml-0.2.5
RUN ./configure
RUN make
RUN make install

WORKDIR /
RUN git clone https://gitlab.maisondelasimulation.fr/pdidev/pdi.git
RUN mkdir pdi/build
WORKDIR  /pdi/build 
RUN cmake -DCMAKE_INSTALL_PREFIX='/opt/pdi' -DBUILD_HDF5_PARALLEL=OFF -DBUILD_PYTHON=ON -DBUILD_PYCALL_PLUGIN=ON -DBUILD_MPI_PLUGIN=OFF ..
RUN make install

WORKDIR /
ADD /gym_dssat_pdi/ .

RUN mkdir dssat-csm-os/build
WORKDIR /dssat-csm-os/build
RUN cmake -DCMAKE_INSTALL_PREFIX='/opt/dssat_pdi' -DCMAKE_PREFIX_PATH='/opt/pdi/share/paraconf/cmake;/opt/pdi/share/pdi/cmake' ..
RUN make
RUN make install

RUN echo 'export PATH=$PATH:/opt/pdi/bin' >> ~/.bashrc
ENV PATH "$PATH:/opt/pdi/bin"

WORKDIR /
RUN cp -r /dssat-csm-data/* /opt/dssat_pdi/

RUN pip3 install matplotlib numpy Jinja2 gym==0.18.3 pyzmq psutil pyyaml pympler

WORKDIR /gym-dssat-pdi
RUN pip install -e .
WORKDIR /