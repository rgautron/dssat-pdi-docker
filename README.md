# gym-DSSAT-PDI coupling debug docker image

To use this:

Clone this repository with recursive flag:

```bash
git clone --recurse-submodules https://gitlab.inria.fr/rgautron/dssat-pdi-docker.git
```

To build Docker image:

```bash
docker build . -t "dssat_pdi_debian"
```

To interactive run the Docker image:

```bash
docker run -it dssat_pdi_debian /bin/bash
```